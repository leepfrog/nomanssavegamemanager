# README #

---
**Please note regarding NEXT compatibility:** 

NMSGM was developed around 2016 and worked with the way NMS handled 
save games at that time. There project is currently not actively developed.

It has come to my attention that with the release of NMS NEXT the game handles 
the saves differently which **results in this version of NMSGM not working for that version.**

There are forks attempting to perform the necessary changes to the code to support NEXT, e.g. by 
Vendolis: https://bitbucket.org/Vendolis/nomanssavegamemanager/src/master/

If you are looking for a version working with NEXT please check out those forks.

---

## Project information ##

This is the sourcecode running behind No Man's SaveGame Manager made available for everyone to look at, modify or use for their own purposes.

Details on this can be found on these links:

* https://nomansskymods.com/mods/no-mans-savegame-manager/
* https://www.reddit.com/r/NoMansSkyTheGame/comments/4yqknr/no_mans_savegame_manager_nmsgm_beta_release/

## Building ##
The source code should build fine if used in VS2015 having nuget enabled.

## License ##
Copyright (c) 2016 leepfrog

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.